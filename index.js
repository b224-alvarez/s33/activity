//4.
fetch("https://jsonplaceholder.typicode.com/todos")
  .then((response) => response.json())
  .then((data) => {
    let titles = data.map((elem) => {
      return elem.title;
    });
    console.table(titles);
  });

//5.
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "GET",
  headers: { "Content-type": "application/json" },
})
  .then((response) => response.json())
  .then((json) => console.log(json));

//6.
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "GET",
  headers: { "Content-type": "application/json" },
})
  .then((response) => response.json())
  .then((json) =>
    console.log(
      `The item ${json.title} on the list has a status of ${json.completed}`
    )
  );

//7.
fetch("https://jsonplaceholder.typicode.com/todos", {
  method: "POST",
  headers: { "Content-type": "application/json" },
  body: JSON.stringify({
    completed: false,
    id: 201,
    title: "Created To Do List Item",
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

//8.
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PUT",
  headers: { "Content-type": "application/json" },
  body: JSON.stringify({
    dateCompleted: "Pending",
    description: "To update the my to do list with a different data structure",
    id: 1,
    status: "Pending",
    title: "Updated To Do List Item",
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

//9.
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PUT",
  headers: { "Content-type": "application/json" },
  body: JSON.stringify({
    completed: false,
    dateCompleted: "11/29/22",
    id: 1,
    status: "Complete",
    title: "delectus aut autem",
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

//10.
fetch("https://jsonplaceholder.typicode.com/todos/2", {
  method: "PATCH",
  body: JSON.stringify({
    title: "lorem ipsum",
  }),
  headers: {
    "Content-type": "application/json",
  },
})
  .then((response) => response.json())
  .then((json) => console.log(json));

//11.
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PUT",
  body: JSON.stringify({
    completed: true,
    wasChanged: "11/29/22",
  }),
  headers: {
    "Content-type": "application/json",
  },
})
  .then((response) => response.json())
  .then((json) => console.log(json));

//12.
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "DELETE",
});
